#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>

using namespace std;
using namespace ros;

Publisher actPub;
std::string team;

Rate rat(50.0);

void p2sCallback(const std_msgs::Bool &msg) {
    std_msgs::UInt8 teamID;
    if (team.compare("yellow") == 0) {
        teamID.data = 1;
    } else if (team.compare("blue") == 0) {
        teamID.data = 2;
    } else {
        teamID.data = 3;
    }
    actPub.publish(teamID);
}

int main(int argc, char **argv)
{
    init(argc, argv, "Little Bot Activation");

    NodeHandle nh;

    Subscriber p2sSub = nh.subscribe("/pull_to_start", 1, p2sCallback);
    actPub = nh.advertise<std_msgs::UInt8>("/act", 1);

    while (nh.ok())
    {
        nh.getParam("team", team);
        rat.sleep();
        spinOnce();
    }
    

    return 0;
}
