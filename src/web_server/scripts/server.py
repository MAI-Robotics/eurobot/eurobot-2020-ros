import os, sys
sys.path.insert(0,os.path.dirname(os.path.realpath(__file__)))

import time
import random
from server_config import *

should_activate_experiment = False

def __log(status="INFO", message=""):
    print("[{status}] {message}".format(status=status, message=message))

    ## logging
    # app.logger.debug("Logging debug messages")
    # app.logger.warning("Logging warnings")
    # app.logger.error("Logging erros")

@app.route("/")
def index():
    return render_template("index.html", logged_in=False)