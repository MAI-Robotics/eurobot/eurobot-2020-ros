var switch_img_p0 = document.getElementById("switch-img-p0");
var switch_img_p1 = document.getElementById("switch-img-p1");
var switch_img_i0 = document.getElementById("switch-img-i0");
var switch_img_i1 = document.getElementById("switch-img-i1");
var img_to_change = document.getElementById("img-to-change");

switch_img_i0.onclick = function() {
    img_to_change.src = getDomain() + ":8001/stream?topic=/camera/image0"
}

switch_img_i1.onclick = function() {
    img_to_change.src = getDomain() + ":8001/stream?topic=/camera/image1"
}

switch_img_p0.onclick = function() {
    img_to_change.src = getDomain() + ":8001/stream?topic=/camera/processed0"
}

switch_img_p1.onclick = function() {
    img_to_change.src = getDomain() + ":8001/stream?topic=/camera/processed1"
}

function setStandardUrl() {
    img_to_change.src = getDomain() + ":8001/stream?topic=/camera/processed0"
}

function getDomain() {
    var url = window.location.href;
    var splitted = url.split(":");
    var domain = splitted[0] + ":" + splitted[1];
    console.log(domain);
    return domain;
}
