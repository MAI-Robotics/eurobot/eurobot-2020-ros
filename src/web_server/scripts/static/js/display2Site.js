var score = document.getElementById("score");

function update_robot_score()
{
    getJSON(window.location.origin + "/get_robot_score", (status, xhr)=>{
        var data = xhr.response;
        score.innerHTML   = (data.s).toFixed(1).toString();
    });
}

setInterval(update_robot_score, 1000);

var robot_started = document.getElementById("robot_started");

function update_robot_started()
{
    getJSON(window.location.origin + "/get_pull_to_start", (status, xhr)=>{
        var data = xhr.response;
        robot_started.innerHTML   = (data.p2s).toString();
    });
}

setInterval(update_robot_started, 1000);