#!/usr/bin/env python

import os
import rospy
from std_msgs.msg import String, Bool, Float32, UInt8
from sensor_msgs.msg import Range
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist

from tf.transformations import euler_from_quaternion, quaternion_from_euler

import json
from server import *

def __log(status="INFO", message=""):
    rospy.loginfo("[{status}] {message}".format(status=status, message=message))

__log("initializing node {}".format(NODE_NAME))


rospy.init_node("{}".format(NODE_NAME), anonymous=False)

@app.route("/shutdown")
def shutdown():
    rospy.loginfo("[INFO] Shutting Down")

    #os.system("sh /home/odroid/scripts/shutdown_script.sh &")
    #os.system("disown")
    rospy.loginfo("[INFO] shutting down now....")

    try:
        thread.exit()
    except:
        __log("Failed to exit server thread ...")

    return "Shutting Down"
rospy.on_shutdown(shutdown)

robot_position = {
            "x"   : 0.0,
            "y"   : 0.0,
            "yaw" : 0.0
        }

pull_to_start_state = False
robot_score = 0.0
    
def pull_to_start_callback(msg):
    global pull_to_start_state

    pull_to_start_state = msg.data

rospy.Subscriber('pull_to_start', Bool, pull_to_start_callback)

def robot_score_callback(msg):
    global robot_score

    robot_score = msg.data

rospy.Subscriber('robot_score', Float32, robot_score_callback)

def update_robot_position(odometry_msg):
    robot_pose = odometry_msg.pose.pose

    _, _, robot_position["yaw"] = euler_from_quaternion([robot_pose.orientation.x, robot_pose.orientation.y, robot_pose.orientation.z, robot_pose.orientation.w])
    robot_position["x"] = robot_pose.position.x
    robot_position["y"] = robot_pose.position.y

reset_drive_train_pub = rospy.Publisher('/reset_drive_train', Bool, queue_size=10)
eurobot_task_cmd_pub = rospy.Publisher('/eurobot_task_cmd', String, queue_size=10)
activate_lil_bot_pub = rospy.Publisher('/act', UInt8, queue_size=1)

rospy.Subscriber("odom", Odometry, update_robot_position)

@app.route("/reset_task")
def reset_task():
    eurobot_task_cmd_pub.publish(String("reset"))
    return "Reseting Task"

@app.route("/test_robot")
def test_robot():
    try:
        side = request.args.get("side").decode("utf-8")
        cmd = "test_{}".format(side)
        rospy.loginfo("testing with command {}".format(cmd))
        eurobot_task_cmd_pub.publish(String(cmd))
    except:
        pass
        
    return "Testing Robot"

@app.route("/start_task")
def start_task():
    try:
        side = request.args.get("side").decode("utf-8")
        cmd = "start_{}".format(side)
        rospy.loginfo("starting side with command {}".format(cmd))
        eurobot_task_cmd_pub.publish(String(cmd))
    except:
        pass
    
    return "Starting Task"

@app.route("/eurobot_task_reset")
def eurobot_task_reset():
    msg = String()
    msg.data = "reset"
    eurobot_task_cmd_pub.publish(msg)
    return "Eurobot Task Reset"

@app.route("/activate_lil_bot")
def activate_lil_bot():
    msg = UInt8()
    team = rospy.get_param("team")
    if team.equals("yellow"):
        msg.data = 1
    elif team.equals("blue"):
        msg.data = 2
    else:
        msg.data = 3
    
    activate_lil_bot_pub.publish(msg)
    return "Activate Little Bot"

@app.route("/eurobot_kill_task")
def eurobot_kill_task():
    msg = String()
    msg.data = "kill_task"
    eurobot_task_cmd_pub.publish(msg)
    return "Eurobot Kill Task"

@app.route("/set_yellow")
def eurobot_start_yellow():
    rospy.set_param("team", "yellow")
    return "Set Team Yellow"

@app.route("/set_blue")
def eurobot_start_purple():
    rospy.set_param("team", "blue")
    return "Set Team Yellow"

@app.route("/reset_odometry")
def reset_odometry():
    msg = Bool()
    msg.data = True
    reset_drive_train_pub.publish(msg)
    return "Odometry Reset"

@app.route("/get_robot_position")
def get_robot_position():
    return json.dumps(robot_position)

@app.route("/get_robot_score")
def get_robot_score():
    global robot_score
    
    data = {"s":robot_score}
    return json.dumps(data)

@app.route("/get_pull_to_start")
def get_pull_to_start():
    global pull_to_start_state

    data = {"p2s":pull_to_start_state}
    return json.dumps(data)


gripper0_state = False
toggle_gripper0_pub = rospy.Publisher('/servos/arm/gripper0/state', Bool, queue_size=0)
gripper1_state = False
toggle_gripper1_pub = rospy.Publisher('/servos/arm/gripper1/state', Bool, queue_size=0)
gripper2_state = False
toggle_gripper2_pub = rospy.Publisher('/servos/arm/gripper2/state', Bool, queue_size=0)
gripper3_state = False
toggle_gripper3_pub = rospy.Publisher('/servos/arm/gripper3/state', Bool, queue_size=0)
gripper4_state = False
toggle_gripper4_pub = rospy.Publisher('/servos/arm/gripper4/state', Bool, queue_size=0)

@app.route("/open_gripper")
def open_gripper():
    msg = Bool(True)
    toggle_gripper0_pub.publish(msg)
    toggle_gripper1_pub.publish(msg)
    toggle_gripper2_pub.publish(msg)
    toggle_gripper3_pub.publish(msg)
    toggle_gripper4_pub.publish(msg)
    return "Gripper Opened"

@app.route("/close_gripper")
def close_gripper():
    msg = Bool(False)
    toggle_gripper0_pub.publish(msg)
    toggle_gripper1_pub.publish(msg)
    toggle_gripper2_pub.publish(msg)
    toggle_gripper3_pub.publish(msg)
    toggle_gripper4_pub.publish(msg)
    return "Gripper Closed"

@app.route("/toggle_gripper0")
def toggle_gripper0():
    msg = Bool(not gripper4_state)
    toggle_gripper0_pub.publish(msg)
    return "Gripper 0 Toggled"


@app.route("/toggle_gripper1")
def toggle_gripper1():
    msg = Bool(not gripper4_state)
    toggle_gripper1_pub.publish(msg)
    return "Gripper 1 Toggled"


@app.route("/toggle_gripper2")
def toggle_gripper2():
    msg = Bool(not gripper4_state)
    toggle_gripper2_pub.publish(msg)
    return "Gripper 2 Toggled"


@app.route("/toggle_gripper3")
def toggle_gripper3():
    msg = Bool(not gripper4_state)
    toggle_gripper3_pub.publish(msg)
    return "Gripper 3 Toggled"


@app.route("/toggle_gripper4")
def toggle_gripper4():
    msg = Bool(not gripper4_state)
    toggle_gripper4_pub.publish(msg)
    return "Gripper 4 Toggled"

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.after_request
def allow_cors(response):
    response.headers['Access-Control-Allow-Origin'] = "*"
    return response

__log("Starting web server")
thread.start_new_thread(app.run, (HOST, PORT))

time.sleep(5)
__log("Server should be running")

rospy.spin()

