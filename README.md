# Eurobot 2020 - Sail the world

## Working repo

This is the working repository for MAI-Members
You can use this code but please contact me beforehand, so that we know who is using it

### Guidelines for commiting

- Please give a message that is useful if you changed something
- Please do not delete code without permission
- Please check if compiling before commmiting
